function mkEl(tag, parent, atts = {}) {
  const el = document.createElement(tag)
  parent.appendChild(el)
  for (let key in atts) {
    el[key] = atts[key]
  }
  return el
}

function mkBox(x, y, w, h) {
  console.log(map)
  const box = mkEl("b", map)
  box.x = x
  box.y = y
  box.w = w
  box.h = h
  return box
}

const boxes = [
  mkBox(0, 45, 1000, 10),
  mkBox(20, 20, 20, 5)
]

const player = mkEl('p', map)
player.x = 40
player.y = 30
player.w = 5
player.h = 15
player.vY = 0
player.vX = 0
player.way = 0

window.addEventListener('keydown', (ev)=> {
  if (ev.keyCode==39) player.way = 1
  if (ev.keyCode==37) player.way = -1
  if (ev.keyCode==38 && canJump()) player.vY = -10
})

window.addEventListener('keyup', (ev)=> {
  if (ev.keyCode==39 && player.way == 1)  player.way = 0
  if (ev.keyCode==37 && player.way == -1) player.way = 0
})

function canJump() {
  return true
}

function updatePlayer() {
  player.vX += player.way
  player.vX *= .75
  player.vY += 1
  player.vY *= .9
  player.x += player.vX
  player.y += player.vY
  const colision = testColision(player)
  if (colision.y === 1) {
    player.vY *= -.7
    player.y = colision.el.y - player.h
  }
}

function testColision(obj) {
  const all = allObjects()
  for (let el, i = 0; (el = all[i]); i++) {
    const innerColision = ()=> obj.x > el.x && obj.x < el.x+el.w
    const rightColision = ()=> obj.x < el.x && obj.x+obj.w > el.x
    const leftColision = ()=> obj.x+obj.w > el.x+el.w && obj.x < el.x+el.w
    if (el !== obj) {
      // botton colision
      if (obj.y + obj.h > el.y && obj.y < el.y) {
        let right = rightColision()
        let left = leftColision()
        if (left || right || innerColision()) {
          return { el, y: 1, x: left?1:right?-1:0 }
        }
      }
      // top colision
      if (obj.y < el.y+el.h && obj.y+obj.h > el.y) {
        let right = rightColision()
        let left = leftColision()
        if (left || right || innerColision()) {
          return { el, y: -1, x: left?1:right?-1:0 }
        }
      }
    }
  }
  return {}
}

function allObjects() {
  return [...boxes, player]
}

function tic() {
  setTimeout(tic, 33);
  const all = allObjects()
  updatePlayer()
  for (let el, i = 0; (el = all[i]); i++) {
    el.style.left = el.x + "vw"
    el.style.top = el.y + "vw"
    el.style.width = el.w + "vw"
    el.style.height = el.h + "vw"
  }
}
tic()
