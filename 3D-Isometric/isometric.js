const lado = 60

const worldEl = document.querySelector('w')

for (let y = -8; y < 8; y++) for (let x = -8; x < 8; x++) {
  let b = document.createElement('b')
  b.innerText = `${x},${y}`
  b.style.top = y*lado+'px'
  b.style.left = x*lado+'px'
  b.style.zIndex = -9*lado*2
  b.style.background = (x+y)%2 === 0 ? '#080' : '#390'
  worldEl.appendChild(b)
}

function mkParalelepipedo(x, y) {
  const p = document.createElement('p')
  p.x = 0
  p.y = 0
  paredeSul(p, 0, 0)
  paredeLeste(p, 0, 0)
  teto(p, 0, 0)
  worldEl.appendChild(p)
  p.move = (incX, incY)=> {
    console.log('MOVE', incX, incY)
    p.x += incX
    p.y += incY
    p.style.top = p.y*lado+'px'
    p.style.left = p.x*lado+'px'
    p.style.zIndex = ~~((p.x+p.y)*lado)
  }
  p.move(x, y)
  return p
}

function paredeSul(parent, x, y) {
  let parede = document.createElement('b')
  parede.style.top = y*lado+'px'
  parede.style.left = x*lado+'px'
  parede.style.transform = `translateX(-${lado/2}px) skewX(45deg)`
  parede.style.background = 'rgba(255,0,0,.8)'
  parent.appendChild(parede)
}

function paredeLeste(parent, x, y) {
  let parede = document.createElement('b')
  parede.style.top = y*lado+'px'
  parede.style.left = x*lado+'px'
  parede.style.transform = `translateY(-${lado/2}px) skewY(45deg)`
  parede.style.background = 'rgba(0,0,255,.8)'
  parent.appendChild(parede)
}

function teto(parent, x, y) {
  let parede = document.createElement('b')
  parede.style.top = (y-1)*lado+'px'
  parede.style.left = (x-1)*lado+'px'
  parede.style.background = 'rgba(200,0,255,.8)'
  parent.appendChild(parede)
}

function mkSprited(sX, sY, mX, mY) {
  const b = document.createElement('s')
  b.x = 0
  b.y = 0
  b.style.backgroundImage = 'url(sprites.png)'

  worldEl.appendChild(b)
  b.change = (newX, newY)=> {
    b.style.backgroundPosition = `${newX*lado}px -${newY*lado}px`
  }
  b.change(sX, sY)
  b.move = (incX, incY)=> {
    console.log('MOVE', incX, incY)
    b.x += incX
    b.y += incY
    b.style.top = b.y*lado+'px'
    b.style.left = b.x*lado+'px'
    b.style.zIndex = ~~((b.x+b.y)*lado)
  }
  b.move(mX, mY)
  return b
}

mkParalelepipedo(-4, -4)

mkParalelepipedo(0, 0)
mkParalelepipedo(1, 0)
mkParalelepipedo(2, 0)
mkParalelepipedo(2, 1)
mkParalelepipedo(2, -1)

const box = mkParalelepipedo(3.5, 0)

mkSprited(0, 0, -4, 1)
mkSprited(0, 0, -3, 1)
mkSprited(0, 0, -2, 1)

const doll = mkSprited(0, 1, -2, 3)

let dollStep = 0
function moveDoll(incX, incY) {
  dollStep++
  if (dollStep>5) dollStep=0
  doll.change(~~(dollStep/3), 1)
  doll.move(incX/lado, incY/lado)
}

window.addEventListener('keydown', ev=> {
  ev.preventDefault()
  if (ev.key === 'ArrowUp') moveDoll(0, -2)
  if (ev.key === 'ArrowDown') moveDoll(0, 2)
  if (ev.key === 'ArrowLeft') moveDoll(-2, 0)
  if (ev.key === 'ArrowRight') moveDoll(2, 0)
})
